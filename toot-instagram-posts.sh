#!/bin/bash

if [[ $# -lt 1 ]]; then
  echo "USAGE: $0 [your Instagram username]"
  exit
fi

CMD="$HOME/go/bin/madonctl"
IG_USER="$1"
MAIN_DIR=$(dirname "$0")
TMP_DIR="$MAIN_DIR/tmp"
JSON_FILE="$TMP_DIR/$IG_USER.json"
TS_FILE="$MAIN_DIR/last_processed_timestamp.txt"

echo -n "=== Starting @ "; date
echo "- Setting Instagram user to $IG_USER"

# Set up tmp folder if needed
[ -d "$TMP_DIR" ] || mkdir "$TMP_DIR"

# Download my most recent Instagram posts
curl --silent "https://sebsauvage.net/rss-bridge/?action=display&media_type=all&format=Json&bridge=Instagram&u=$IG_USER" --output "$JSON_FILE"
echo "- Downloaded Instagram JSON file"

# If/when the RSS Bridge has a hiccup, back off for now
grep "The requested resource cannot be found" "$JSON_FILE" && exit 1

MAX_IDX=$(jq -r ".items | length - 1" "$JSON_FILE")
for IDX in $(seq $MAX_IDX -1 0); do
  echo "- Considering Instagram post $IDX"

  # Get the timestamp, we'll use it for keeping track of what was tooted already
  POST_TS=$(jq -r ".items[$IDX].date_modified" "$JSON_FILE")
  LAST_TOOTED_TS=$(cat "$TS_FILE")

  # Skip posts older than the most recently tooted one
  if [[ -f "$TS_FILE" ]]; then
    if [[ "$LAST_TOOTED_TS" == "$POST_TS" || "$LAST_TOOTED_TS" > "$POST_TS" ]]; then
      echo "  - Already tooted $IDX ($POST_TS), skipping"
      continue
    fi
  fi

  # Extract one post from the JSON structure
  POST=$(jq -r ".items[$IDX]" "$JSON_FILE")

  # Get the text from the IG post. The `.title` node doesn't contain the full
  # text, tho, so we have to pull it from the first `alt` attribute. ¯\_(ツ)_/¯
  BLURB=$(echo $POST | jq -r '.content_html' | sed 's/<.*>//')
  if [[ -z "$BLURB" ]]; then
    BLURB=$(echo $POST | jq -r '.content_html | capture(" alt=\"(?<alt>.+?)\"").alt')
  fi
  MEDIA_IDS=""

  # The blurb might contain HTML entities, and Mastodon will post them as is,
  # and… listen, I am not proud of this, okay?
  BLURB=$(echo $BLURB | php -R 'echo html_entity_decode($argn);')

  # Download max 4 attachments (images) or else the Mastodon instance will balk
  MAX_ATT_IDX=$(echo $POST | jq -r ".attachments | [length - 1, 3] | min")
  for ATT_IDX in $(seq 0 $MAX_ATT_IDX); do
    ATT=$(echo $POST | jq -r ".attachments[$ATT_IDX]")
    FILE_URL=$(echo $ATT | jq -r '.url')
    FILE_EXT=$(echo $ATT | jq -r '.mime_type | split("/") | last')
    TMP_FILE="$TMP_DIR/$IDX-$ATT_IDX.$FILE_EXT"

    # Download the IG media file
    curl --silent "$FILE_URL" --output "$TMP_FILE"
    echo "- Downloaded $TMP_FILE"

    # Upload to Mastodon instance
    MEDIA_ID=$($CMD media --output json --file "$TMP_FILE" | jq -r '.id')
    MEDIA_IDS="$MEDIA_IDS,$MEDIA_ID"
    echo "- Uploaded $TMP_FILE → media ID $MEDIA_ID"
    rm "$TMP_FILE"
  done

  # Remove leading comma from list of media IDs
  MEDIA_IDS=$(echo $MEDIA_IDS | sed 's/^,//')

  # Toot away
  $CMD toot --media-ids "$MEDIA_IDS" "$BLURB"


  # Store just processed post POST_TS for later use in freshness checks
  echo "$POST_TS" > "$TS_FILE"
done

echo -n "=== Done @ "; date
