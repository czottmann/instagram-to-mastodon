# Turn Instagram posts into Mastodon toots

This is a simple Mastodon helper to repost my recent Instagram post. I don't
consider it a bot since I tend to run it manually whenever I made a new
Instagram post which isn't often.

I cobbled this script together in about two hours, start to finish, so don't
expect the world, please.

Made to work and tested on both macOS Mojave and a 2018 Raspberry Pi.


## Usage

Run

```bash
./toot-instagram-posts.sh [your Instagram username]
```

The script will fetch the most recent public posts for the passed Instagram
user, and post them as toots (containing the actual image as media) to your
Mastodon account. They're not just pointers to the original IG post, they'll be
more or less exact copies, and won't contain references back to the IG post,
either.

The script will prevent double-posting by storing the most recent IG post
timestamp in the project's folder in a file called
`last_processed_timestamp.txt`.

It does handle multi-image IG posts. However, my Mastodon instance
(mastodon.social) set the max number of media files per post to 4 so if an
Instagram post has more than that it will silently drop the rest.


## Liability

Yeah… none. Use at your own risk.  ;)


## Prerequisites

- bash 4
- a fully configured and working instance of
  [madonctl](https://github.com/McKael/madonctl) for controlling your Mastodon
  account
- [jq](https://stedolan.github.io/jq/) for parsing the Instagram feed JSON file
- a recent version of PHP (nothing fancy, the one coming w/ your OS is probably
  fine) for unescaping HTML entities


## Author

Carlo Zottmann, carlo@zottmann.org,
[@czottmann@mastodon.social](https://mastodon.social/@czottmann),
https://gitlab.com/czottmann/instagram-to-mastodon


## Useful to you?

[Feel free to toot "hi"!](https://mastodon.social/@czottmann)


## Acknowledgements

Thanks to whoever runs this
[public RSS Bridge instance](https://sebsauvage.net/rss-bridge/)! :)

I know we _should_ all be using federated IG competitors but I really don't
(yet) so here we go.


## License

    DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
    Version 2, December 2004

    Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

    Everyone is permitted to copy and distribute verbatim or modified
    copies of this license document, and changing it is allowed as long
    as the name is changed.

    DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

    0. You just DO WHAT THE FUCK YOU WANT TO.
